## Transforming GCP Credentials service account - Local
Prepare the Google credentials service account json file to be set as an Environment Variable without any new lines. 

- You can use the following substitution commands to replace the newlines in `vi` with a blank. 
  - You can use the following substitution commands to do all that in vi:
    1. Hit `Esc` in vi, then enter `1,$s/\n//`
    1. Enter `:`, then `wq` to save and quit
  - Here are the full set of `vi` commands:
```
:1,$s/\n//
:wq
```

- Alternatively, you can do the following global substitutions in Atom or another text editor:
    - Replace each newline with a blank. (In Atom, click the .* button in the Find control and then replace \n with a blank value. But then deselect the .* button before making the remaining substitutions.)
  - Save the path to transformed file in an environment variable:

Now you can use the transformed file with terraform:
```
export GOOGLE_CREDENTIALS="/path/to/transformed-gcp-credentials-file"
```
Thanks to Roger Berlind for figuring this out in [set-variables-script](https://github.com/hashicorp/terraform-guides/tree/master/operations/variable-scripts#running-the-set-variablessh-script).

